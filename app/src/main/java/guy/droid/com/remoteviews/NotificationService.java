package guy.droid.com.remoteviews;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.widget.RemoteViews;
import android.widget.Toast;

/**
 * Created by admin on 9/24/2016.
 */

public class NotificationService extends Service {


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction().equals("PLAY")) {

            Toast.makeText(this, "PLAY", Toast.LENGTH_SHORT).show();

        }else if (intent.getAction().equals("PAUSE")) {

            Toast.makeText(this, "PAUSE", Toast.LENGTH_SHORT).show();

        }else if (intent.getAction().equals("STOP")) {

            Toast.makeText(this, "STOP", Toast.LENGTH_SHORT).show();
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            mNotificationManager.cancel(0);

        }else if (intent.getAction().equals("FRS")) {

            showNotification();

        }

        else
        {

            Toast.makeText(this, ""+intent.getAction(), Toast.LENGTH_SHORT).show();
        }

        return START_NOT_STICKY;
    }
    Notification status;
    private final String LOG_TAG = "NotificationService";

    private void showNotification() {
        RemoteViews views = new RemoteViews(getPackageName(), R.layout.statusbarlayout);
        views.setImageViewResource(R.id.status_play,R.mipmap.ic_launcher);


        Intent Play = new Intent(this, NotificationService.class);
        Play.setAction("PLAY");
        PendingIntent PlayPend = PendingIntent.getService(this, 0, Play, 0);
        views.setOnClickPendingIntent(R.id.play,PlayPend);

        Intent Pause = new Intent(this, NotificationService.class);
        Play.setAction("PAUSE");
        PendingIntent PausePend = PendingIntent.getService(this, 0, Play, 0);
        views.setOnClickPendingIntent(R.id.pause,PausePend);

        Intent Stop = new Intent(this, NotificationService.class);
        Play.setAction("STOP");
        PendingIntent StopePend = PendingIntent.getService(this, 0, Play, 0);
        views.setOnClickPendingIntent(R.id.stop,StopePend);




        NotificationCompat.Builder builder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(getApplicationContext())
                        .setVisibility(Notification.VISIBILITY_PUBLIC)
                        .setOngoing(true)
                        .setSmallIcon(R.drawable.abc_ic_menu_share_mtrl_alpha)
                        .setContentTitle("title text")
                        .setContentText("content text");
         builder.setContent(views);
        builder.setCustomBigContentView(views);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, builder.build());
    }

}
